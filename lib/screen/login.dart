import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oninyon/screen/home_screen.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  final String email = 'test@oninyon.com';
  final String pass = 'T3stOninyon';

  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Image(
                    image:
                        NetworkImage('https://oninyon.com/images/bob/logo.png'),
                    width: 200,
                    height: 200,
                  ),
                ),
                const SizedBox(height: 12),
                const Text('Email'),
                const SizedBox(height: 12),
                TextField(
                  controller: _emailController,
                ),
                const SizedBox(height: 12),
                const Text('Password'),
                const SizedBox(height: 12),
                TextField(
                  controller: _passController,
                  obscureText: true,
                ),
                error.isNotEmpty
                    ? const Text('Email atau Password yang anda masukkan salah')
                    : const SizedBox(),
                const SizedBox(height: 36),
                Center(
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        backgroundColor: Colors.cyan),
                    onPressed: () {
                      if (_emailController.text == email &&
                          _passController.text == pass) {
                        Get.to(() => const HomeScreen());
                      }

                      setState(() {
                        error = 'wrong email or password';
                      });
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 0),
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
