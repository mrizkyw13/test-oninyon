import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          decoration: const BoxDecoration(color: Colors.grey),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    'Tentang Kami',
                    style: TextStyle(fontSize: 32),
                  ),
                ),
                Container(
                  decoration: const BoxDecoration(color: Colors.white),
                  child: const DefaultTabController(
                    length: 2,
                    child: Column(
                      children: [
                        TabBar(
                          tabs: [
                            Tab(text: 'Sejarah Perusahaan'),
                            Tab(text: 'Tim Kami'),
                          ],
                        ),
                        TabBarView(children: [
                          Column(
                            children: [
                              Image(
                                  image: NetworkImage(
                                      'https://oninyon.com/images/office/24.jpg'))
                            ],
                          ),
                          Text('Ini Tim Kami')
                        ])
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
