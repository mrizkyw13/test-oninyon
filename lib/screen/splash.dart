// lib/splash_screen.dart
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:get/get.dart';
import 'package:oninyon/screen/login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // Navigate to HomeScreen after 3 seconds
    Timer(const Duration(seconds: 3), () {
      Get.to(() => const LoginPage());
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Add your splash screen content here
            Center(
              child: Image(
                  image:
                      NetworkImage('https://oninyon.com/images/bob/logo.png')),
            ),
            // FlutterLogo(size: 100),
            // SizedBox(height: 20),
            // Text(
            //   'Welcome to MyApp',
            //   style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            // ),
          ],
        ),
      ),
    );
  }
}
